# Fallout Solver #

For I fun I decided to create a solver for Fallout terminal hacking.

### Setup ###

* download the java files
* complile the files
* run using: **$ java FalloutMain**

### Interactive Mode ###

Use interactive mode to find the terminal password while playing the game

* enter the password options
* when all password options are entered press "I" 
* The program will generate the best guess. Enter the guess into the game.
* Enter the number of correct matching letters.
* Repeat until the correct password is solved

### Simulation Mode ###

Used mainly for testing. In the future I would like to use it, along with some "challenging" data sets to test the limits of the solver. I would like to see how big a list of password options and word sizes could grow while and still be solved in four guesses. *Currently I've tested that it can find the password for 180 7-letter words.*

* enter the password options
* when all password options are entered press "S" 
* the program will chose one of the password options randomly
* then it will attempt to find the password. it will print either a success or failure message

*Feel free to fork and make any improvements that you think would be cool*