/**
 * Terminal simulator. Intit with a dictionary
 * and it will randomly pick a word as the password.
 * It handles the guess submission as well.
 * @author Nate DiPiazza
 *
 */
public class Terminal {
  private Dictionary passwordOptions;
  private Word password;
  //private Random rand = new Random();
  private int guessesLeft = 4;

  public Terminal(Dictionary passwordOptions) {
    super();
    this.passwordOptions = passwordOptions;
    pickPassword();
  }
  
  public int submitGuess(Word guess){
    if(guessesLeft == 0){
      return -1;
    }
    guessesLeft--;
    int numCorrect = passwordOptions.compare(guess, password);
    return numCorrect;
  }
  
  /**
   * Randomly pick a password from passwordOptions
   */
  private void pickPassword(){
    int passSize = passwordOptions.getDictionarySize();
    // select a word to be the password
    int randomInt = randInt(passSize);
    password = passwordOptions.selectByIndex(randomInt);
  }
  
  public int getGuessesLeft() {
    return guessesLeft;
  }

  /**
   * Pick a random int
   * to be called by pickPassword
   * @param min
   * @param max
   * @return random int
   */
  private int randInt(int max) {
    int randomNum = (int)(Math.random()*max-1);
    return randomNum;
  }
  
}
