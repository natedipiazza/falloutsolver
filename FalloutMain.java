import java.util.Scanner;


public class FalloutMain {

  /**
   * Main Class ***************************************
   * Handles keyboard input as well as initiating solver
   * and terminal logic. The results will be printed
   * to the console.
   * @param args
   * @throws Exception 
   */
  public static void main(String[] args) throws Exception {
    
    Dictionary dictionary = null;
    boolean interactive = false;
    int numGuesses = 4;
    int numCorrect = -1;
    // Interface enter next word: press Q to quit, press S to run simulation mode and I to run Interactive mode with game
    System.out.println("Fallout 3 password solver. For entertainment purposes only.\nEnter first password possibility:");
    Scanner scanner = new Scanner(System.in);
    while(true){
      String posPass = scanner.nextLine();
      if(posPass.equals("Q")){
        quit();
      }else if(posPass.equals("S") || posPass.equals("I")){
        interactive = posPass.equals("I") ? true : false;
        break;
      }
      if(dictionary == null){
        dictionary = new Dictionary(posPass);
        prompt();
      } else {
        if(dictionary.addWord(posPass)){
          prompt();
        }else{
          System.out.println("Incorrect word size. Try again.");
        }
      }
      
    }
    // Program then calls guess() and reduce() iteratively until
    Word guess;
    // Interactive Mode /////////////////////////////////////
    if(interactive){
      for (int i = 0; i < numGuesses; i++) {
        numCorrect = -1; //reset input var
        guess = dictionary.guess();
        dictionary.pop(guess);
        System.out.println("Guess: " + guess.toString());
        while(numCorrect < 0){
          System.out.println("Enter number of correct letters: ");
          String inputText = scanner.nextLine();
          // still allow user to quit if they want to
          if(inputText.equals("Q")){
            quit();
          }
         //catch bad input for numericality. prompt user if invalid
          try {
            numCorrect = new Integer(inputText);
            
          } catch (NumberFormatException e) {
            numCorrect = -1;
          }
          
          if(numCorrect > dictionary.getWordSize()){
            numCorrect = -1;
          }
          // give error mess if negative
          if(numCorrect < 0){
            System.err.println("Invalid Format.");
          }
        }
       
        if(numCorrect == dictionary.getWordSize()){
          exactMatchMsg(guess);
          return; // We are done :)
        }
        // use what we know to refine our next guess
        dictionary.refine(guess, numCorrect);
      }
    // Terminal Simulator Block ////////////////////////////
    }else{
      //create a terminal with selected password
      //for us to hack
      Terminal terminal = new Terminal(dictionary);
      do {
        guess = dictionary.guess();
        dictionary.pop(guess);
        numCorrect = terminal.submitGuess(guess);
        if(numCorrect == dictionary.getWordSize()){
          exactMatchMsg(guess);
          return; // We are done :)
        }
        // use what we know to refine our next guess
        dictionary.refine(guess, numCorrect);
        System.out.println("Incorrect. Number of matches: " + numCorrect);
      } while (terminal.getGuessesLeft() > 0);
    }
    // if reached here we failed to find the password
    failureMsg();
  }
  
  static void prompt(){
    System.out.println("Enter Q to quit. Enter S to run simulation and I to use interactively with game.");
    System.out.println("Enter next password possibility:");
  }
  
  static void exactMatchMsg(Word password){
    // TODO get exact game verbage
    System.out.println(password.toString() +  " is the password. Logging in as Administrator.");
  }
  
  static void failureMsg(){
    System.out.println("Failed to find the password.");
  }
  
  static void quit(){
    System.out.println("Quitting");
    System.exit(0);
  }

}
