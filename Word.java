/**
 * Basic container class for
 * a dictionary word. Stores
 * basic data including is sum(kmer)
 * matches in the dictionary.
 * @author Nate DiPiazza
 *
 */
public class Word {
  
  private String text;
  private int kmerTotal;
  private int wordSize;
  private char[] charArray;

  public Word(String text) {
    super();
    this.text = text;
    wordSize = text.length();
    charArray = text.toCharArray();
  }
  /**
   * return word in string format
   */
  public String toString(){
    return text;
  }
  /**
   * @return number of chars in this word
   */
  public int size() {
    return wordSize;
  }


  public char[] getArray() {
    return charArray;
  }
  /**
   * The sum of all kmers for each word in the dictionary.
   * @return kmerTotal
   */
  public int getKmerTotal() {
    return kmerTotal;
  }

  public void setKmerTotal(int kmerTotal) {
    this.kmerTotal = kmerTotal;
  }
  
}
