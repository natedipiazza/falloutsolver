import java.util.ArrayList;

/**
 * A list of Words. Also contains the solver logic functions.
 * All added Words must be the same size.
 * @author Nate DiPiazza
 */
public class Dictionary {
  
  private ArrayList<Word> wordList = new ArrayList<Word>();
  private final int wordSize;
  
  /**
   * Constructor *****************************************
   * add first word to dictionary and set wordSize constant
   * @param text
   */
  public Dictionary(String text) {
    Word word = new Word(text);
    wordList.add(word);
    wordSize = text.length();
    addWord(text);
  }
  /**
   * It takes as input a string and creates a word,
   * which it then adds to the dictionary. Unless
   * the word is not the correct size.
   * @param text
   * @return True or False if word is correct size
   */
  public boolean addWord(String text){
    boolean sameSize = true;
    Word word = new Word(text);
    // do not add if not the same size
    if(word.size() != wordSize){
      sameSize = false;
    }else{
      wordList.add(word); 
    }
    return sameSize;
  }
  /**
   * number of password options
   * that we guess from.
   * @return
   */
  public int getDictionarySize(){
    return wordList.size();
  }
  /**
   * The number of letters in a given word.
   * <br>
   * <i>Every word in dictionary is the
   * same size.</i>
   * @return
   */
  public int getWordSize(){
    return wordSize;
  }
  /**
   * Top level function used to select a password
   * guess from the dictionary of password options
   * @return
   */
  public Word guess(){
    calcKmerTotals();
    return getMaxKmer();
  }
  
  /**
   * Refine the dictionary
   * by eliminating words
   * that cannot be the
   * password.
   * 
   * If the guess matched m letters and
   * the word has n letters in common with
   * the guess, eliminate a word if n != m
   * <br>
   * guess returned by guess() method, number of matching chars
   * given by actual game or returned by terminal object.
   * @param
   * @throws Exception 
   */
  public void refine(Word guess, int numMatch) throws Exception{
    ArrayList<Word> removedWords = new ArrayList<Word>();
    int numCommon;
    for (Word word : wordList){
      numCommon = compare(guess, word);
      if(numCommon != numMatch){
        removedWords.add(word);
      }
    }
    // now remove the words
    for (Word rWord : removedWords){
      wordList.remove(rWord);
    }
    if(wordList.size() == 0){
      throw new FalloutException("Dictionary is empty. There can be no feasable solution!");
    }
  }
  /**
   * Remove word from dictionary
   * @param word
   */
  public void pop(Word word){
    wordList.remove(word);
  }
  
  public Word selectByIndex(int i){
    return wordList.get(i);
  }
  /**
   * Compare two Words.
   * Return the number of matching chars
   * @param wordA
   * @param wordB
   * @return kmer
   */
  public int compare(Word wordA, Word wordB){
    int kmer = 0;
    char[] arrA = wordA.getArray();
    char[] arrB = wordB.getArray();
    for (int i = 0; i < wordA.size(); i++) {
      if(arrA[i] == arrB[i]){
        kmer++;
      }
    }
    return kmer;
  }
  /**
   * Calculate all the kmers totals for every
   * Word in the dictionary
   */
  private void calcKmerTotals(){
    for (Word wordA : wordList){
      int total = 0;
      for (Word wordB : wordList){
        // don't compare against itself
        if(!wordB.toString().equals(wordA.toString())){
          total += compare(wordA, wordB);
        }
      }
      // this is the number of kmers for all words
      wordA.setKmerTotal(total);
    }
  }
  /**
   * The word that has the most letters
   * in common with all the other words
   * in the dictionary
   * @return max Word
   */
  private Word getMaxKmer(){
    int max = 0;
    Word maxWord = null;
    for (Word word : wordList){
      // tie by convention goes to latest word
      if(word.getKmerTotal() >= max){
        max = word.getKmerTotal();
        maxWord = word;
      }
    }
    return maxWord;
  }
  
}
