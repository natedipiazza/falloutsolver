/**
 * Very basic Exception class
 * used to raise exceptions when
 * bad input issues arise
 * @author Nate DiPiazza
 *
 */
public class FalloutException extends Exception {

  private static final long serialVersionUID = 9214167306164430745L;

  public FalloutException(String message){
     super(message);
  }

}
